import 'package:flutter/material.dart';
import 'package:flutter_application_3/code1.dart';

void main() => runApp(const MyApp());

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
        fontFamily: 'Oswald', // font Default
      ),
      home: const ResponsivePage(), // Panggil FirstScreen di sini
    );
  }
}

//=========== Margin dan pedding =====================

// class FirstScreen extends StatelessWidget {
//   const FirstScreen({Key? key}) : super(key: key);

//   @override
//   Widget build(BuildContext context) {
//     return Scaffold(
//       appBar: AppBar(
//         title: const Text('First Screen'),
//       ),
//       body: Container(
//         color: Colors.blue,
//         padding: const EdgeInsets.all(10),
//         margin: const EdgeInsets.all(10),
//         child: const Text(
//           'Hi',
//           style: TextStyle(fontSize: 40),
//         ),
//       ),
//     );
//   }
// }

//=========== Container =====================

// class FirstScreen extends StatelessWidget {
//   const FirstScreen({Key? key}) : super(key: key);

//   @override
//   Widget build(BuildContext context) {
//     return Scaffold(
//       appBar: AppBar(
//         title: const Text('First Screen'),
//       ),
//       body: Container(
//         decoration: BoxDecoration(
//           color: Colors.red,
//           // shape: BoxShape.circle, <-- untuk bentuk backgroundnya
//           boxShadow: const [
//             BoxShadow(
//               color: Colors.black,
//               offset: Offset(3, 4),
//               blurRadius: 10,
//             ),
//           ],
//           border: Border.all(color: Colors.black, width: 2),
//           borderRadius: BorderRadius.circular(10),
//         ),
//         child: const Text(
//           'Hi',
//           style: TextStyle(fontSize: 40),
//         ),
//       ),
//     );
//   }
// }

//=========== row =====================

// class FirstScreen extends StatelessWidget {
//   const FirstScreen({Key? key}) : super(key: key);

//   @override
//   Widget build(BuildContext context) {
//     return Scaffold(
//       appBar: AppBar(
//         title: const Text('First Screen'),
//       ),
//       body: const Row(
//         mainAxisAlignment: MainAxisAlignment.spaceEvenly,
//         children: <Widget>[
//           Icon(Icons.share),
//           Icon(Icons.thumb_up),
//           Icon(Icons.thumb_down),
//         ],
//       ),
//     );
//   }
// }

//=========== column =====================

// class FirstScreen extends StatelessWidget {
//   const FirstScreen({Key? key}) : super(key: key);

//   @override
//   Widget build(BuildContext context) {
//     return Scaffold(
//       appBar: AppBar(
//         title: const Text('First Screen'),
//       ),
//       body: const Column(
//         children: <Widget>[
//           Text(
//             'Sebuah Judul',
//             style: TextStyle(fontSize: 32, fontWeight: FontWeight.bold),
//           ),
//           Text('Lorem ipsum dolor sit amet'),
//         ],
//       ),
//     );
//   }
// }

//=========== button =====================

// class FirstScreen extends StatelessWidget {
//   const FirstScreen({Key? key}) : super(key: key);

//   @override
//   Widget build(BuildContext context) {
//     return Scaffold(
//       appBar: AppBar(
//         title: const Text('First Screen'),
//       ),
//       body: TextButton(
//         child: const Text('Text Button'),
//         onPressed: () {
//           // Aksi ketika button diklik
//         },
//       ),
//       body: OutlinedButton(
//         child: const Text('Outlined Button'),
//         onPressed: () {
//           // Aksi ketika button diklik
//         },
//       ),
//       body: ElevatedButton(
//         child: const Text("Tombol"),
//         onPressed: () {
//           // Aksi ketika button diklik
//         },
//       ),
//       body: IconButton(
//         icon: const Icon(Icons.volume_up),
//         tooltip: 'Increase volume by 10',
//         onPressed: () {
//           // Aksi ketika button diklik
//         },
//       ),
//       body: IconButton(
//         icon: const Icon(Icons.volume_up),
//         tooltip: 'Increase volume by 10',
//         onPressed: () {
//           // Aksi ketika button diklik
//         },
//       ),
//     );
//   }
// }

//=========== DropdownMenu =====================

// class FirstScreen extends StatefulWidget {
//   const FirstScreen({Key? key}) : super(key: key);

//   @override
//   State<FirstScreen> createState() => _FirstScreenState();
// }

// class _FirstScreenState extends State<FirstScreen> {
//   String? language;

//   @override
//   Widget build(BuildContext context) {
//     return Scaffold(
//       appBar: AppBar(
//         title: const Text('First Screen'),
//       ),
//       body: DropdownButton<String>(
//         items: const <DropdownMenuItem<String>>[
//           DropdownMenuItem<String>(
//             value: 'Dart',
//             child: Text('Dart'),
//           ),
//           DropdownMenuItem<String>(
//             value: 'Kotlin',
//             child: Text('Kotlin'),
//           ),
//           DropdownMenuItem<String>(
//             value: 'Swift',
//             child: Text('Swift'),
//           ),
//         ],
//         value: language,
//         hint: const Text('Select Language'),
//         onChanged: (String? value) {
//           setState(() {
//             language = value;
//           });
//         },
//       ),
//     );
//   }
// }

// =========== Input Widget =====================

// class FirstScreen extends StatefulWidget {
//   const FirstScreen({Key? key}) : super(key: key);

//   @override
//   State<FirstScreen> createState() => _FirstScreenState();
// }

// class _FirstScreenState extends State<FirstScreen> {
//   final TextEditingController _name = TextEditingController();

//   @override
//   Widget build(BuildContext context) {
//     return Scaffold(
//       appBar: AppBar(
//         title: const Text('First Screen'),
//       ),
//       body: Padding(
//         padding: const EdgeInsets.all(16.0),
//         child: Column(
//           children: [
//             TextField(
//               controller: _name,
//               decoration: const InputDecoration(
//                 hintText: 'Write your name here...',
//                 labelText: 'Your Name',
//               ),
//             ),
//             const SizedBox(height: 20),
//             ElevatedButton(
//               child: const Text('Submit'),
//               onPressed: () {
//                 showDialog(
//                     context: context,
//                     builder: (context) {
//                       return AlertDialog(
//                         content: Text('Hello, ${_name.text}'),
//                       );
//                     });
//               },
//             )
//           ],
//         ),
//       ),
//     );
//   }

//   @override
//   void dispose() {
//     _name.dispose();
//     super.dispose();
//   }
// }

// =========== Switch Widget =====================

class FirstScreen extends StatefulWidget {
  const FirstScreen({Key? key}) : super(key: key);

  @override
  State<FirstScreen> createState() => _FirstScreenState();
}

class _FirstScreenState extends State<FirstScreen> {
  bool lightOn = false;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('First Screen'),
      ),
      body: Switch(
        value: lightOn,
        onChanged: (bool value) {
          setState(() {
            lightOn = value;
          });

          ScaffoldMessenger.of(context).showSnackBar(
            SnackBar(
              content: Text(lightOn ? 'Light On' : 'Light Off'),
              duration: const Duration(seconds: 1),
            ),
          );
        },
      ),
    );
  }
}

// =========== Radio Widget =====================

// class FirstScreen extends StatefulWidget {
//   const FirstScreen({Key? key}) : super(key: key);

//   @override
//   State<FirstScreen> createState() => _FirstScreenState();
// }

// class _FirstScreenState extends State<FirstScreen> {
//   String? language;

//   @override
//   Widget build(BuildContext context) {
//     return Scaffold(
//       appBar: AppBar(
//         title: const Text('First Screen'),
//       ),
//       body: Column(
//         mainAxisSize: MainAxisSize.min,
//         children: <Widget>[
//           ListTile(
//             leading: Radio<String>(
//               value: 'Dart',
//               groupValue: language,
//               onChanged: (String? value) {
//                 setState(() {
//                   language = value;
//                   showSnackbar();
//                 });
//               },
//             ),
//             title: const Text('Dart'),
//           ),
//           ListTile(
//             leading: Radio<String>(
//               value: 'Kotlin',
//               groupValue: language,
//               onChanged: (String? value) {
//                 setState(() {
//                   language = value;
//                   showSnackbar();
//                 });
//               },
//             ),
//             title: const Text('Kotlin'),
//           ),
//           ListTile(
//             leading: Radio<String>(
//               value: 'Swift',
//               groupValue: language,
//               onChanged: (String? value) {
//                 setState(() {
//                   language = value;
//                   showSnackbar();
//                 });
//               },
//             ),
//             title: const Text('Swift'),
//           ),
//         ],
//       ),
//     );
//   }

//   void showSnackbar() {
//     ScaffoldMessenger.of(context).showSnackBar(
//       SnackBar(
//         content: Text('$language selected'),
//         duration: const Duration(seconds: 1),
//       ),
//     );
//   }
// }

// =========== Checkbox Widget =====================

// class FirstScreen extends StatefulWidget {
//   const FirstScreen({Key? key}) : super(key: key);

//   @override
//   State<FirstScreen> createState() => _FirstScreenState();
// }

// class _FirstScreenState extends State<FirstScreen> {
//   bool agree = false;

//   @override
//   Widget build(BuildContext context) {
//     return Scaffold(
//       appBar: AppBar(
//         title: const Text('First Screen'),
//       ),
//       body: ListTile(
//         leading: Checkbox(
//           value: agree,
//           onChanged: (bool? value) {
//             setState(() {
//               agree = value!;
//             });
//           },
//         ),
//         title: const Text('Agree / Disagree'),
//       ),
//     );
//   }
// }

// =========== Image Form Network =====================

// class FirstScreen extends StatelessWidget {
//   const FirstScreen({Key? key}) : super(key: key);

//   @override
//   Widget build(BuildContext context) {
//     return Scaffold(
//       appBar: AppBar(
//         title: const Text('First Screen'),
//       ),
//       body: Center(
//         child: Image.network(
//           'https://picsum.photos/200/300',
//           width: 200,
//           height: 200,
//         ),
//       ),
//     );
//   }
// }

// =========== Image Form Asset =====================

//jangan lupa tambahkan flutter assets pada pubspec.yaml dan buat folder images untuk menampung gambar

// class FirstScreen extends StatelessWidget {
//   const FirstScreen({Key? key}) : super(key: key);

//   @override
//   Widget build(BuildContext context) {
//     return Scaffold(
//       appBar: AppBar(
//         title: const Text('First Screen'),
//       ),
//       body: Center(
//         child: Image.asset('images/tes.png', width: 200, height: 200),
//       ),
//     );
//   }
// }

// =========== Font Form Asset =====================

//jangan lupa tambahkan flutter fonts pada pubspec.yaml dan buat folder fonts untuk menampung file font

// class FirstScreen extends StatelessWidget {
//   const FirstScreen({Key? key}) : super(key: key);

//   @override
//   Widget build(BuildContext context) {
//     return Scaffold(
//       appBar: AppBar(
//         title: const Text('First Screen'),
//       ),
//       body: const Text(
//         'Custom Font',
//         style: TextStyle(
//           fontFamily: 'Oswald',
//           fontSize: 30,
//         ),
//       ),
//     );
//   }
// }

// =========== Navigationa =====================

// =========== untuk pindah pindah halaman biasa =====================

// class FirstScreen extends StatelessWidget {
//   const FirstScreen({Key? key}) : super(key: key);

//   @override
//   Widget build(BuildContext context) {
//     return Scaffold(
//       appBar: AppBar(
//         title: const Text('First Screen'),
//       ),
//       body: Center(
//         child: ElevatedButton(
//           child: const Text('Pindah Screen'),
//           onPressed: () {
//             Navigator.push(context, MaterialPageRoute(builder: (context) {
//               return const SecondScreen();
//             }));
//           },
//         ),
//       ),
//     );
//   }
// }

// class SecondScreen extends StatelessWidget {
//   const SecondScreen({Key? key}) : super(key: key);

//   @override
//   Widget build(BuildContext context) {
//     return Scaffold(
//       appBar: AppBar(
//         title: const Text('Second Screen'),
//       ),
//       body: Center(
//         child: OutlinedButton(
//           child: const Text('Kembali'),
//           onPressed: () {
//             Navigator.pop(context);
//           },
//         ),
//       ),
//     );
//   }
// }

// =========== untuk pindah pindah halaman dengan data =====================

// class FirstScreen extends StatelessWidget {
//   const FirstScreen({Key? key}) : super(key: key);

//   final String message = 'Hello from First Screen!';

//   @override
//   Widget build(BuildContext context) {
//     return Scaffold(
//       appBar: AppBar(
//         title: const Text('First Screen'),
//       ),
//       body: Center(
//         child: ElevatedButton(
//           child: const Text('Pindah Screen'),
//           onPressed: () {
//             Navigator.push(context,
//                 MaterialPageRoute(builder: (context) => SecondScreen(message)));
//           },
//         ),
//       ),
//     );
//   }
// }

// class SecondScreen extends StatelessWidget {
//   final String message;
//   const SecondScreen(this.message, {Key? key}) : super(key: key);

//   @override
//   Widget build(BuildContext context) {
//     return Scaffold(
//       appBar: AppBar(
//         title: const Text('Second Screen'),
//       ),
//       body: Center(
//         child: Column(
//           mainAxisAlignment: MainAxisAlignment.center,
//           children: [
//             Text(message),
//             OutlinedButton(
//               child: const Text('Kembali'),
//               onPressed: () {
//                 Navigator.pop(context);
//               },
//             ),
//           ],
//         ),
//       ),
//     );
//   }
// }
